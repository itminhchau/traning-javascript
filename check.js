class Person{
    constructor(firstName, lastName, birthday, gender){
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.gender = gender;
    }
    get getAge(){
        let age = 0;
        let current=new Date();
        if(this.birthday.month>4){
            age = current.getFullYear()  - 1 - this.birthday.year;
        }
        else if(this.birthday.month==4&&this.birthday.day<16){
            age = current.getFullYear() - this.birthday.year;
        }
        else if(this.birthday.month==4&&this.birthday.day>16){
            age = current.getFullYear() - this.birthday.year -1 ;
        }
        else if(this.birthday.month<4){
            age = current.getFullYear() - this.birthday.year;
        }
        return age;
    }
    
}
let person1 = new Person("Thanh","Ngo",{day: 17,month : 4, year : 2000},"male")
console.log(person1.getAge);