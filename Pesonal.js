class Personal{
    constructor(ID,fullName, email, birthday,position, seniority){
        this.ID=ID;
        this.fullName=fullName;
        this.email=email;
        this.birthday=birthday;
        this.position=position;
        this.seniority=seniority;
    }
    get age(){
        let age=0;
        let currentDate= new Date();
        age=currentDate.getFullYear()-this.birthday.year;
         if(this.birthday.month>currentDate.getMonth()+1)
         {
             age--;
         }else if(this.birthday.month==currentDate.getMonth()+1 && this.birthday.day>currentDate.getDate()){
             age--;
         }
        return age;
    }
  
    get salary(){
       
        let salaryByPosition={
            manager:1000,
            secterary:400,
            employee:300
        }
        let basicSalary=salaryByPosition[this.position.toLowerCase()];
        if (basicSalary==undefined) {
            return "nhap sai roi ong noi";
        }
        switch (true) {
                    case this.seniority<12:
                        return basicSalary* 1.2;
                    case this.seniority<24:
                        return basicSalary* 2;
                    case this.seniority<36:
                        return basicSalary* 3.4;
                    default:
                        return basicSalary* 4.5;
                }
    }
    static highestSalary(personal){
        let arr=[];
        personal = personal.sort((slry1,slry2)=>slry1.salary<slry2.salary);
        arr.push(personal[0]);
        personal.forEach((element,index) => {
            if (index==0) {
                return ;
            }
            if (arr[0].salary==element) {
                arr.push(element);
            }
        });
        return arr;
    }
}

let personal= [new Personal(121,"Dao minh chau","daominhchau@gmail.com",{day:24,month:01,year:1997},"Manager",37),
new Personal(121,"pham thi kieu phuong","kieuphong@gmail.com",{day:7,month:07,year:1997},"secterary",10)    
]  
personal.forEach(element => {
    console.log(element.fullName);
    console.log(`Age: ${element.age}`);
    console.log(`Salary: ${element.salary}`); 
      
});
console.log("Personal Highest salary:\n",Personal.highestSalary(personal));