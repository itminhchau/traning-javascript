class Animal{
    // function khởi tao giá trị ban đau
    constructor(name, age, spec){
        this.name=name;
        this.age=age;
        this.spec=spec;
    }
    speak()
    {
        return "meow meow";
    }
    // lấy giá trị ra
    get getAge(){
        return this.age;
    }
    // xử lý cát intance khác nhau
    static sumCatAge(cat1, cat2){
        return cat1.age+ cat2.age;
    }
}
class Dog extends Animal{
    constructor(name, age,gender,spec="dog"){
        // super goi lai ham cha Animal;
        super(name, age, spec);
        this.gender=gender;
    }
    speak(){
        return "whow whow"
    }
}

let cat1= new Animal("paw",2,"cat");
let cat2= new Animal("paw",3,"cat");
let dog1= new Dog("milu",5,"male");
console.log(dog1);
console.log(dog1.hasOwnProperty("name"));


// console.log(cat1);
// // check cat 1
// console.log(cat1 instanceof Animal);
// // goi method in class
// console.log(cat1.speak());
// console.log(cat1.getAge);
// console.log(Animal.sumCatAge(cat1,cat2));
// Animal.prototype.color ="black";
// console.log(cat1.color);




