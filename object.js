let arr = [1,2,3];
arr[0];
//let object={name: ' abc', age: 18, fly(){return 'dao minh chau'}};
// transfer value variable-> object;
let name ="abc";
let age=18;
let object3={name, age};
//truyen value object -> variable
let object={name:"abc", age: 18}
let name= object.name;
let age= object.age;
let{name, age}= object;
console.log(object.fly());
// arr;
arr2= Array.from(arr1);// khi arr 2 thay doi nhung arr 1 khong thay doi

//object
let object2= Object.assign({},object);// khai bao nhung khi object 2 thay doi object 1 khong thay doi
// check property in object;
console.log('name' in object);
// check property exits in object
object.name !== undefined;
object.hasOwnProperty('name');
// Get keys from object
Object.keys(object)
// get values from object
console.log(Object.values(object));
//
Object.keys(object).forEach(key=>{
    console.log(key);
    
});//
let object={
    name:"abc",
    age: 18,
    birthday:{day: 12,month:9,year: 2000},
    fly(){
        console.log(`${this.name}`);
        
    }
}
let json= JSON.stringify(object);
// convert object qua js
console.log(JSON.stringify(object));
// covert json qua object
object= JSON.parse(json);



